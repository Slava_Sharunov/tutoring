#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h> 

__global__ void colonel(int *a_d){
  *a_d += 1;
}

int main(){

  int a = 0, *a_d;
  //TODO make this program running on GPU
  
  colonel<<<1000,1000>>>(a_d); 
  


  printf("a = %d\n", a);
  cudaFree(a_d);

}
