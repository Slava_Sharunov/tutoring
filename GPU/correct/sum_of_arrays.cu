#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#define N 1000000

//gridDim.x - number of blocks in the current grid
//blockDim.x - number of threads per block

__global__ void AddVector(int *a, int *b, int *c, int num)
{
	int bid = blockIdx.x*blockDim.x + threadIdx.x;
	if (bid<num)
		c[bid]=a[bid]+b[bid];
}

int main()
{	
	int a[N], b[N], c[N];
	int *a_d, *b_d, *c_d;
	int i;

	for(i=0;i<N;i++)
	{
		a[i]=i;
		b[i]=i*i;
		c[i]=0;
	}

	cudaMalloc((void**)&a_d, N*sizeof(int));
	cudaMalloc((void**)&b_d, N*sizeof(int));
	cudaMalloc((void**)&c_d, N*sizeof(int));
	cudaMemcpy(a_d,&a,N*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(b_d,&b,N*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(c_d,&c,N*sizeof(int),cudaMemcpyHostToDevice);

	float   elapsedTime;
  	cudaEvent_t start, stop;
  	cudaEventCreate(&start);
  	cudaEventCreate(&stop);
  	cudaEventRecord( start, 0 );

	AddVector<<<200,50>>>(a_d,b_d,c_d,N);

	cudaEventRecord( stop, 0 );
  	cudaEventSynchronize( stop );
  	cudaEventElapsedTime( &elapsedTime, start, stop );
  	cudaEventDestroy( start );
  	cudaEventDestroy( stop );
  	printf("GPU Time elapsed: %f milliseconds\n", elapsedTime);
	cudaMemcpy(&c,c_d,N*sizeof(int),cudaMemcpyDeviceToHost);

	for(i=0;i<N;i++)
	{
		printf("%d + %d = %d\n",a[i],b[i],c[i]);
	}

	cudaFree(a_d);
	cudaFree(b_d);
	cudaFree(c_d);
	return 0;
}
