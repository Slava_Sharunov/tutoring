// It takes the string "Hello ", prints it, then passes it to CUDA with an array
// of offsets. Then the offsets are added in parallel to produce the string "World!"
 
#include <stdio.h>

const int N = 7;
const int blocksize = 7;


__global__
void hello(char *a, int *b)
{
 a[threadIdx.x] += b[threadIdx.x];
}

int main()
{
 char a[N] = "Hello ";
 int b[N] = {15, 10, 6, 0, -11, 1, 0};

 char *ad;
 int *bd;
 const int csize = N*sizeof(char);
 const int isize = N*sizeof(int);

 printf("%s", a);
 //TODO
 //Allocate a memory in your device

 cudaMalloc( (void**)&ad, csize );
 cudaMalloc( (void**)&bd, isize );
 cudaMemcpy( ad, a, csize, cudaMemcpyHostToDevice );
 cudaMemcpy( bd, b, isize, cudaMemcpyHostToDevice );

 //Create Grid for execution
 dim3 dimBlock( blocksize, 1 );
 dim3 dimGrid( 1, 1 );

 //invoke your device-function

 hello<<<dimGrid, dimBlock>>>(ad, bd);

 //Free the device-memory

 cudaMemcpy( a, ad, csize, cudaMemcpyDeviceToHost );
 cudaFree( ad );
  
 //TODO_END
 printf("%s\n", a);
 return EXIT_SUCCESS;
}
