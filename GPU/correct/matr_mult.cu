#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define BLOCK_SIZE 16

typedef struct {
	int width;
	int height;
	int* elements;
} Matrix;

__global__ void MatMulKernel(Matrix A, Matrix B, Matrix C) {
// Each thread computes one element of C
// by accumulating results into C value
	int Cvalue = 0;
	int i;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	if(row > A.height || col > B.width) return;
    
	for (i = 0; i < A.width; i++)
		Cvalue += (A.elements[row * A.width + i]) * (B.elements[i * B.width + col]);
	C.elements[row * C.width + col] = Cvalue;
}

void MatMul(const Matrix A, const Matrix B, Matrix C) {
//TODO
// Load A and B to device memory
	Matrix d_A;
	d_A.width = A.width;
	d_A.height = A.height;
	cudaMalloc(&d_A.elements, A.width * A.height * sizeof(int));
	cudaMemcpy(d_A.elements, A.elements, A.width * A.height * sizeof(int), cudaMemcpyHostToDevice);
	
        Matrix d_B;
	d_B.width = B.width;
	d_B.height = B.height;
	cudaMalloc(&d_B.elements, B.width * B.height * sizeof(int));
	cudaMemcpy(d_B.elements, B.elements, B.width * B.height * sizeof(int), cudaMemcpyHostToDevice);
	
// Allocate C in device memory

	Matrix d_C;
	d_C.width = C.width;
	d_C.height = C.height;
	cudaMalloc(&d_C.elements, C.width * C.height * sizeof(int));
	
// Invoke kernel

	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid((B.width + dimBlock.x - 1) / dimBlock.x, (A.height + dimBlock.y - 1) / dimBlock.y);
	MatMulKernel<<<dimGrid, dimBlock>>>(d_A, d_B, d_C);
	
// Read C from device memory

	cudaMemcpy(C.elements, d_C.elements, C.width * C.height * sizeof(int), cudaMemcpyDeviceToHost);

// Free device memory

	cudaFree(d_A.elements);
	cudaFree(d_B.elements);
	cudaFree(d_C.elements);
}

void printMatrix(Matrix mat)
{
   int i,j;
   for(i = 0; i < min(10, mat.height); i++)
	{
		for(j = 0; j < min(10, mat.width); j++)
			printf("%3d ", mat.elements[i*mat.width + j]);
		printf("\n");
	}
	printf("\n");
}

void fillMatrix(Matrix mat)
{
  int i,j;
  for(i = 0; i < mat.height; i++)
		for(j = 0; j < mat.width; j++)
			mat.elements[i*mat.width + j] = (rand()%10) - 5;
}


int main(int argc, char* argv[]){
	Matrix A, B, C;
	int a1, a2, b1, b2;
        if (argc == 4 && isdigit(argv[1][0]) && isdigit(argv[2][0])
			&& isdigit(argv[3][0])) {
       		a1 = atoi(argv[1]); /* Height of A */
		a2 = atoi(argv[2]); /* Width of A */
		b1 = a2;            /* Height of B */
		b2 = atoi(argv[3]); /* Width of B */
    }else {
        printf("USAGE\n   ./app [A height] [A width] [B width] \n");
        return 0;
    }
	A.height = a1;
	A.width = a2;
	A.elements = (int*)malloc(A.width * A.height * sizeof(int));
	
	B.height = b1;
	B.width = b2;
	B.elements = (int*)malloc(B.width * B.height * sizeof(int));

	C.height = A.height;
	C.width = B.width;
	C.elements = (int*)malloc(C.width * C.height * sizeof(int));
	
        fillMatrix(A);
        fillMatrix(B);
	
	// Matrix multiplication kernel 
	MatMul(A, B, C);
  
        printf("Matrix A:\n");
	printMatrix(A);
	printf("Matrix B:\n");
        printMatrix(B);
	printf("Matrix C:\n");
        printMatrix(C);
}
