#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>


//compile like: nvcc integral_cuda.cu -arch sm_20 -o integral_cuda
__device__ float FUN(float x)
{
   return 8.0f+2.0f*x-x*x;
}

__global__ void CalcIntegral(int * a, float * h, float *integral)
{
  
  int ID = blockIdx.x*blockDim.x + threadIdx.x;
 
  atomicAdd(integral, FUN(a[0] + h[0] * (ID + 0.5)));
 

}


int main(int argc, char* argv[])
{
  float integral=0.0;
  float *integral_d;
  int a,b,N;
  int * a_d;
  float h;
  float * h_d;
  

  a = -2;
  b = 4; 
  N = 100000;
  h=(float)(b-a)/N;

  cudaMalloc((void**)&a_d, sizeof(int));
  cudaMalloc((void**)&h_d, sizeof(float));
  cudaMalloc((void**)&integral_d, sizeof(float));
  cudaMemcpy(a_d, &a, sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(h_d, &h, sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(integral_d, &integral, sizeof(float), cudaMemcpyHostToDevice);

  float   elapsedTime;
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord( start, 0 );	

  CalcIntegral<<<100,1000>>>(a_d,h_d,integral_d);

  cudaEventRecord( stop, 0 );
  cudaEventSynchronize( stop );
  cudaEventElapsedTime( &elapsedTime, start, stop );
  cudaEventDestroy( start );
  cudaEventDestroy( stop );
  printf("GPU Time elapsed: %f seconds\n", elapsedTime/1000.0);

  cudaMemcpy(&integral, integral_d ,sizeof(float), cudaMemcpyDeviceToHost);
 
  printf("The value of the integral is: %f \n", integral*h);

  return 0;
}
