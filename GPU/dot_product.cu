#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#define N 10
#define THREADS_PER_BLOCK 5

__global__ void thread_dot( int *a, int *b, int *c ) 
{
      //TODO
	
}

__global__ void block_dot( int *a, int *b, int *c ) 
{
       //TODO
       
}

void printVector(int *arr)
{
  int i;
  for(i=0;i<N;i++)
  {
     printf("%d ",arr[i]);
  }
  printf("\n");
	
}

int main()
{
    int *a, *b, c;
    int *a_d,*b_d,*c_d;
    int i;

    a = (int*) malloc(N*sizeof(int));
    b = (int*) malloc(N*sizeof(int));
    c = 0;

    for(i=0;i<N;i++)
    {
    	a[i]=i;
    	b[i]=2*i;
    }

        cudaMalloc((void**)&a_d, N*sizeof(int));
	cudaMalloc((void**)&b_d, N*sizeof(int));
	cudaMalloc((void**)&c_d, sizeof(int));

	cudaMemcpy(a_d, a, N*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(b_d, b, N*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(c_d, &c, sizeof(int), cudaMemcpyHostToDevice);

	thread_dot<<<1,N>>>(a_d,b_d,c_d);
	//block_dot<<<N/THREADS_PER_BLOCK,THREADS_PER_BLOCK>>>( a_d, b_d, c_d );

	cudaMemcpy(&c,c_d,sizeof(int),cudaMemcpyDeviceToHost);
    printf("A: ");
    printVector(a);
    printf("B: ");
    printVector(b);

    printf("Result of A*B: %d \n",c);

    free(a);
    free(b);

    cudaFree(a_d);
    cudaFree(b_d);
    cudaFree(c_d);


	return 0;
}
