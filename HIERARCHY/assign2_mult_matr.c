#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#define N 2000

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int ** createMatrix() {
	int i;
	int ** matrix = (int**) calloc(N,sizeof(int*));
	int * m = (int*) calloc(N*N,sizeof(int));
	for (i = 0; i < N; i++) {
		matrix[i] = m+(i*N);
	}
	return matrix;
}

void fillMatrix(int ** matrix) {
	int i;
	for (i = 0; i < N*N; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

void printMatrix(int ** matrix)
{
	int i,j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<N;j++)
			printf("%2d ",matrix[i][j]);
		printf("\n");
	}
}

int ** matrixmult_usual(int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix();
    //TODO
    
    return C;
}

int ** matrixmult_spatial(int ** A, int ** B) //both matrices we "read" by rows
{
    int ** C = (int**)createMatrix();
    //TODO

    return C;
}

void main()
{
	struct timeval stop, start;
	float elapsed;
	int result;
	int ** a, **b, **c, **c_other;
	

 	a = (int**)createMatrix();
 	fillMatrix(a);
 	b = (int**)createMatrix();
 	fillMatrix(b);
        

 	gettimeofday(&start, 0);
	c = matrixmult_usual(a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
   	printf("Code executed for \"USUAL\" version in %f milliseconds.\n", elapsed); 

    	gettimeofday(&start, 0);
	c_other = matrixmult_spatial(a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
    	printf("Code executed for \"SPATIAL\" version in %f milliseconds.\n", elapsed);
}
