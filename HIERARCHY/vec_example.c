#define N 200000

// compie: gcc -c -O2 -ftree-vectorize -fopt-info-vec-all vec_example.c

double a[N] __attribute__(( aligned(16) ));
double b[N] __attribute__(( aligned(16) )) ;
double c[N] __attribute__(( aligned(16) )) ;

void f()
{ 
    int i;

    for( i = 0; i < N; i++ )
    {
        a[i] = b[i] + c[i];
    }
}

int main( int argc,char** argv )
{
    f();

    return( 0 );
}
