#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

void main()
{
	int N = 100000;
	int a[N], b[N], c[N];
 	int i;
 	struct timeval stop, start;
 	float elapsed;

  	for (i=0; i<N; i++){
    	a[i] = i*100;
    	b[i]= i*500;
  }
    gettimeofday(&start, 0);
    for(i=0;i<N;i++)
    	c[i]=a[i]*b[i];
    gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);

    printf("Code executed in %f milliseconds.\n", elapsed);

}
