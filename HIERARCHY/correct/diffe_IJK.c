#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int ** createMatrix(int n) {
	int i;
	int ** matrix = (int**) calloc(n,sizeof(int*));
	int * m = (int*) calloc(n*n,sizeof(int));
	for (i = 0; i < n; i++) {
		matrix[i] = m+(i*n);
	}
	return matrix;
}

void fillMatrix(int n, int ** matrix) {
	int i;
	for (i = 0; i < n*n; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

void printMatrix(int n, int ** matrix)
{
	int i,j;
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
			printf("%3d ",matrix[i][j]);
		printf("\n");
	}
}

int ** matrixmultIJK(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (i = 0; i < size; i++)
        for (j = 0; j < size; j++)
            for (k = 0; k < size; k++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}

int ** matrixmultIKJ(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (i = 0; i < size; i++)
        for (k = 0; k < size; k++)
            for (j = 0; j < size; j++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}

int ** matrixmultJIK(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (j = 0; j < size; j++)
        for (i = 0; i < size; i++)
            for (k = 0; k < size; k++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}

int ** matrixmultJKI(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (j = 0; j < size; j++)
        for (k = 0; k < size; k++)
            for (i = 0; i < size; i++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}

int ** matrixmultKIJ(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (k = 0; k < size; k++)
        for (i = 0; i < size; i++)
            for (j = 0; j < size; j++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}

int ** matrixmultKJI(int size, int ** A, int ** B) //normal multiplication
{
    int ** C = (int**)createMatrix(size);
    int i,j,k;

    int sum;

    for (k = 0; k < size; k++)
        for (j = 0; j < size; j++)
            for (i = 0; i < size; i++)
                C[i][j] += A[i][k]*B[k][j];
    return C;
}


int main(int argc, char* argv[])
{
	int N;
    if (argc == 2 && isdigit(argv[1][0])) {
        N = atoi(argv[1]);
    }else {
        printf("USAGE\n   mult [SIZE] \n");
        return 0;
    }

	struct timeval stop, start;
	float elapsed;
	int result;
	int ** a, **b, **c;

 	a = (int**)createMatrix(N);
 	fillMatrix(N,a);
 	b = (int**)createMatrix(N);
 	fillMatrix(N,b);

	gettimeofday(&start, 0);
	c = matrixmultIJK(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for IJK version in %f milliseconds.\n", elapsed); 

    gettimeofday(&start, 0);
	c = matrixmultIKJ(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for IKJ version in %f milliseconds.\n", elapsed);

    gettimeofday(&start, 0);
	c = matrixmultJIK(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for JIK version in %f milliseconds.\n", elapsed);

    gettimeofday(&start, 0);
	c = matrixmultJKI(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for JKI version in %f milliseconds.\n", elapsed);

    gettimeofday(&start, 0);
	c = matrixmultKIJ(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for KIJ version in %f milliseconds.\n", elapsed);

    gettimeofday(&start, 0);
	c = matrixmultKJI(N,a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
	//printMatrix(N,c);
    printf("Code executed for KJI version in %f milliseconds.\n", elapsed);

    return 0;
}