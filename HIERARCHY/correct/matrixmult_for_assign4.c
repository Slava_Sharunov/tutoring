#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>

#define mSize 2000

double ** createMatrix(int n) {
	int i;
	double ** matrix = (double**) calloc(n,sizeof(double*));
	double * m = (double*) calloc(n*n,sizeof(double));
	for (i = 0; i < n; i++) {
		matrix[i] = m+(i*n);
	}
	return matrix;
}

double ** transposeMatrix(int n, double ** matrix)
{
	int i,j;
	double ** transMatrix = (double**)createMatrix(n);
	double temp;
	for (i=0; i<n; i++) 
  		for (j=0; j<n; j++) 
    		transMatrix[i][j] = matrix[j][i];
    return transMatrix;	
}

void fillMatrix(int n, double ** matrix) {
	int i;
	for (i = 0; i < n*n; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

void printMatrix (int n, double ** matrix)
{
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) 
			printf("%2f ",matrix[i][j]);
		printf("\n");
	}
}

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}



void main()
{
	double ** a, ** b, ** c;
	struct timeval stop, start;
	int i,j,k;
	float elapsed;

	a = (double**)createMatrix(mSize);
	b = (double**)createMatrix(mSize);
	c = (double**)createMatrix(mSize);

	fillMatrix(mSize, a);
	fillMatrix(mSize, b);
   	
	gettimeofday(&start, 0);
  	for (i = 0; i < mSize; i++)
 		for (j = 0; j < mSize; j++)
 			for (k = 0; k < mSize; k++)
 				c[i][j] += a[i][k] * b[k][j];
 		 
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);

        printf("Code executed for normal in %f milliseconds.\n", elapsed); 
	//printf("C norma: \n");
	//printMatrix(mSize,c);

        double * matrixA = (double*) calloc(mSize*mSize,sizeof(double));
        double * matrixB = (double*) calloc(mSize*mSize,sizeof(double));
        double * matrixC = (double*) calloc(mSize*mSize,sizeof(double));

    	for (i=0;i<mSize;i++)
    	   for(j=0;j<mSize;j++)
    	      matrixA[i*mSize+j]=a[i][j];
    		

 
    	for (i=0;i<mSize;i++)
    	   for(j=0;j<mSize;j++)
    	      matrixB[i*mSize+j]=b[i][j];
    		
    	gettimeofday(&start, 0);
        for(i=0;i<mSize;i++)
    	   for(j=0;j<mSize;j++)
    	      for(k=0;k<mSize;k++)
    	         matrixC[i*mSize+j] += matrixA[i*mSize+k]*matrixB[k*mSize+j];
        gettimeofday(&stop,0);
    
        elapsed = timedifference_msec(start, stop);

        printf("Code executed for vector in %f milliseconds.\n", elapsed);
        //printf("C vector: \n");
	/*for (i=0;i<mSize;i++)
    	 {
 	    for(j=0;j<mSize;j++)
               printf("%f ",matrixC[i*mSize+j]); 
            printf("\n");
         }*/		
    
    	double * b1 = (double*) calloc(mSize,sizeof(double));
    	double * b2 = (double*) calloc(mSize,sizeof(double));
    	double * b3 = (double*) calloc(mSize,sizeof(double));
    	double * b4 = (double*) calloc(mSize,sizeof(double));
    	double * b5 = (double*) calloc(mSize,sizeof(double));
    	double t1,t2,t3,t4,t5;
    	double * q = (double*) calloc(mSize,sizeof(double));
	double ** cnew = (double**)createMatrix(mSize);
	
	gettimeofday(&start,0);
    	for (k = 0; k < mSize; k+=5)
	{
 		b1 = &b[k][0]; b2 = &b[k+1][0]; b3 = &b[k+2][0]; b4 = &b[k+3][0]; b5 = &b[k+4][0];
 		for (i = 0; i < mSize; i++)
 		{   
 			q = &cnew[i][0];
 			t1 = a[i][k]; t2=a[i][k+1]; t3=a[i][k+2]; t4=a[i][k+3]; t5=a[i][k+4];
 			for (j = 0; j < mSize; j++)
 			{   
 				q[j] += t1 * b1[j] + t2 * b2[j] + t3 * b3[j] + t4 * b4[j] + t5 * b5[j];
 			}

 		}
	}
	gettimeofday(&stop,0);
    
    	elapsed = timedifference_msec(start, stop);

    	printf("Code executed for super in %f milliseconds.\n", elapsed);
   	//printf("C super: \n");
	//printMatrix(N,cnew);
	 	
}

