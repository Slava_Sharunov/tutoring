#include <mpi.h>
#include "omp.h"
#include <stdio.h>
#include <stdlib.h>


void outMatrix(int *matrix, int n)
{
    int i,j;
     for (i=0;i<n;i++)
          {
           for (j=0;j<n;j++)
               printf("%d ",matrix[i*n+j]);
           printf("\n");
         }
}

void fillMatrix(int *matrix,int n)
{
   int i,j;
   for (i=0;i<n;i++) 
      {
           for (j=0;j<n;j++) 
           {
               matrix[i*n+j] = rand()%3;
           }
     }
}

int main(int argc, char* argv[])  {    
    
    int n;
    if (argc == 2 && isdigit(argv[1][0])) {
        n = atoi(argv[1]);
    }else {
        printf("USAGE\n   mult [SIZE] \n");
        return 0;
    }
    int i,j;       

    int size, rank, nl;
    int sum;

    MPI_Init (& argc ,& argv );
    MPI_Comm_size ( MPI_COMM_WORLD ,& size );
    MPI_Comm_rank ( MPI_COMM_WORLD ,& rank );

    nl = (n-1)/size + 1; //how many rows in matrix we will use for each threads

    int bufa[n*nl], bufb[n*nl], bufc[n*nl];
    

    int * a;
    int * b;
    int * c;

    if (rank == 0)
    { 
      a = (int*) calloc(n*n,sizeof(int));
      b = (int*) calloc(n*n,sizeof(int));
      c = (int*) calloc(n*n,sizeof(int));
      
      fillMatrix(a,n);
      fillMatrix(b,n);
      
      printf("A:\n");
      outMatrix(a,n);
      printf("\n");
      printf("B:\n");
      outMatrix(b,n);
      printf("\n");  
    }
   //TODO - BEGIN
      MPI_Bcast (&n, 1, MPI_INT , 0, MPI_COMM_WORLD );
      MPI_Scatter(a, n*nl, MPI_INT, bufa, n*nl, MPI_INT, 0, MPI_COMM_WORLD);
      MPI_Scatter(b, n*nl, MPI_INT, bufb, n*nl, MPI_INT, 0, MPI_COMM_WORLD);
    
    
    #pragma omp parallel for private(i) num_threads(n)
    for(i=0;i<n*nl;i++)
        bufc[i]=bufa[i]+bufb[i];
    
    MPI_Gather(bufc, n*nl, MPI_INT, c, n*nl, MPI_INT, 0, MPI_COMM_WORLD);
    //TODO - END
    if (rank == 0)
    {
      printf("C:\n");
      outMatrix(c,n);
    }
    MPI_Finalize ();
     return 0;
} 
