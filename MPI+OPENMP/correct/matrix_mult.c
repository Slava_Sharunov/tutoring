#include <mpi.h>
#include "omp.h"
#include <stdio.h>
#include <stdlib.h>


void outMatrix(int *matrix, int n)
{
    int i,j;
     for (i=0;i<n;i++)
          {
           for (j=0;j<n;j++)
               printf("%2d ",matrix[i*n+j]);
           printf("\n");
         }
}

void fillMatrix(int *matrix,int n)
{
   int i,j;
   for (i=0;i<n;i++) 
      {
           for (j=0;j<n;j++) 
           {
               matrix[i*n+j] = rand()%3;
           }
     }
}


int main(int argc, char* argv[])  {    
    
    int n;
    if (argc == 2 && isdigit(argv[1][0])) {
        n = atoi(argv[1]);
    }else {
        printf("USAGE\n   mult [SIZE] \n");
        return 0;
    }
    int i,j,k;       

    int size, rank, nl;
    int sum;

    MPI_Init (& argc ,& argv );
    MPI_Comm_size ( MPI_COMM_WORLD ,& size );
    MPI_Comm_rank ( MPI_COMM_WORLD ,& rank );
    MPI_Datatype element, element_type;
    
    nl = (n-1)/size + 1;
    
    int bufa[n*nl], bufc[n*nl];
    

    int * a;
    int * b;
    int * c;
    b = (int*) calloc(n*n,sizeof(int));

     
    if (rank == 0)
    { 
      a = (int*) calloc(n*n,sizeof(int));
      c = (int*) calloc(n*n,sizeof(int));

      fillMatrix(a,n);
      fillMatrix(b,n);
      printf("A:\n");
      outMatrix(a,n);
      printf("\n");
      printf("B:\n");
      outMatrix(b,n);
      printf("\n"); 
    }

      MPI_Bcast (&n, 1, MPI_INT , 0, MPI_COMM_WORLD );
      MPI_Bcast (b, n*n, MPI_INT , 0, MPI_COMM_WORLD );
           
      MPI_Scatter(a, n*nl, MPI_INT, bufa, n*nl, MPI_INT, 0, MPI_COMM_WORLD);
      
    

    #pragma omp parallel for private(j) num_threads(n) reduction(+:sum)
    for(k=0;k<nl;k++)
    {
      for(j=0;j<n;j++)
      {
        sum=0;
        for(i=0;i<n;i++)
          sum+=bufa[i+n*k]*b[i*n+j];
        bufc[j+n*k] = sum;
      }
    }

    
    MPI_Gather(bufc, n*nl, MPI_INT, c, n*nl, MPI_INT, 0, MPI_COMM_WORLD);
       
    if (rank == 0)
    {
      printf("C:\n");
      outMatrix(c,n);
    }
    MPI_Finalize ();
    return 0;
} 
