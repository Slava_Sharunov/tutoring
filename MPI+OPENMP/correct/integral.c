#include <mpi.h>
#include "omp.h"
#include <stdio.h>
#include <stdlib.h>
#define FUN(x) (8+2*(x)-(x)*(x))
#define N 100000000

double CalcIntegral(double a, double b, int n, int my_rank, int number_threads)
{
  int i;
  double result=0, 
         h=(b - a) / N; //step for Rectangle method
 

  #pragma omp parallel for reduction (+: result) num_threads(number_threads)
  for(i = n*my_rank; i < n*(my_rank+1); i++)
  {
    result += FUN(a + h * (i + 0.5)); //Сompute result in the midpoint and add it to our sum
  }

  result *= h;

  return result;
}


int main(int argc, char* argv[])
{
  double integral, global_integral;
  double w_time;

  int size, rank, nl;

  int number_threads;
    if (argc == 2 && isdigit(argv[1][0])) {
        number_threads = atoi(argv[1]);
    }else {
        printf("USAGE\n   integral [NUMTHREADS] \n");
        return 0;
    }

  MPI_Init (NULL , NULL);
  MPI_Comm_size ( MPI_COMM_WORLD ,& size );
  MPI_Comm_rank ( MPI_COMM_WORLD ,& rank );

  nl = (N-1)/size + 1;

  if (rank==0)
  {
    MPI_Bcast (&nl, 1, MPI_INT , 0, MPI_COMM_WORLD);
  }
  
  integral = CalcIntegral(-2, 4, nl, rank,number_threads);
  MPI_Reduce(&integral, &global_integral, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  
  if (rank == 0)
    {   
         printf("Result for function: %f\n",global_integral); 
    }
    

  MPI_Finalize();
  return 0;
}
