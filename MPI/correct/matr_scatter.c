#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>
#define N 6

void printMatrix(int * matrix)
{
	int i,j;
	for (i=0;i<N;i++)
	{
		for(j=0;j<N;j++)
			printf("%2d ",matrix[i*N+j]);
		printf("\n");
	}
}

void main()
{
	int numtasks, rank, i, nl;
    

    int root=0;
    int *buf;
    int * matrix;
    
	MPI_Init(NULL,NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	nl = (N-1)/numtasks+1;
	buf = (int*) calloc(N*nl,sizeof(int));

	if (rank==root)
	{
		matrix = (int*) calloc(N*N,sizeof(int));
	    int i,j,k=0;
	    for(i=0;i<N;i++)
	    	for(j=0;j<N;j++)
	    	{
	    		matrix[i*N+j]=k;
	    		k++;
	    	}
	    printf("The default matrix:\n");	
        printMatrix(matrix);
	}

	MPI_Scatter(matrix,N*nl,MPI_INT,buf,N*nl,MPI_INT,root,MPI_COMM_WORLD);

	for (i=0;i<N*nl;i++)
		buf[i]=rank*buf[i];

	MPI_Gather(buf,N*nl,MPI_INT,matrix,N*nl,MPI_INT,root,MPI_COMM_WORLD);

	if (rank==root)
	{   
		printf("The \"gathered\" matrix:\n");
		printMatrix(matrix);
	}

	MPI_Finalize();
}
