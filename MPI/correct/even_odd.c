#include "mpi.h"
#include <stdio.h>
int main(int argc, char **argv)
{
   int size, rank, a, b, tag=1;
   MPI_Status Stat;
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   a = rank;
   if (rank==size-1 || rank==size-2)
   {
      MPI_Recv(&b, 1, MPI_INT, rank-2, tag, MPI_COMM_WORLD, &Stat);
      printf("Task %d: Received %d from task %d\n",
         rank, b, Stat.MPI_SOURCE);
   }
   else if(rank==0 || rank==1)
      MPI_Send(&a, 1, MPI_INT, rank+2, tag, MPI_COMM_WORLD);
   else
   {
      MPI_Recv(&b, 1, MPI_INT, rank-2, tag, MPI_COMM_WORLD, &Stat);
      printf("Task %d: Received %d from task %d\n",
         rank, b, Stat.MPI_SOURCE);
      MPI_Send(&a, 1, MPI_INT, rank+2, tag, MPI_COMM_WORLD);
   }
   
   MPI_Finalize();
}