#include <stdio.h>
#include "mpi.h"
#include <math.h>

int Fact(n)
{
	if (n==0 || n==1)
		return 1;
	else
		return n*Fact(n-1);
}

int main(int argc, char *argv[])
{
    int numtasks, rank;

    int root=0;
    int i,n = 30;
    double startwtime,endwtime; 
    double fraction,global_sum,local_sum=0;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank==0)
		startwtime=MPI_Wtime();

	MPI_Bcast(&n, 1, MPI_INT, root, MPI_COMM_WORLD);
	    
    for(i=rank;i<=n;i+=numtasks)
    {   
    	fraction = (double)1/Fact(i);
    	local_sum += fraction;
    }
    
    MPI_Reduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 	if (rank == 0)
    {   
    	printf("Result for # = %d: %f\n",numtasks,global_sum); 
        endwtime = MPI_Wtime();
        printf("Time: %f\n", endwtime-startwtime);      
    }
    

	MPI_Finalize();
	return 0;
}
