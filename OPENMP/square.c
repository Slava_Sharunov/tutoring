#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100

int doWork(int x)
{
   return pow(x,2);
}

int * createArray(int n) {
	int i;
	int * m = (int*) calloc(n,sizeof(int));
	for (i = 0; i < n; i++) {
		m[i] = i;
	}
	return m;
}

void printArray(int * array)
{
    int i;
    for (i=0;i<N;i++)
	printf("%d ",array[i]);
    printf("\n");	
}

int main(void){
	int i;
	double x;
	int * array = createArray(N);
printf("Old array: "); 
printArray(array);
//TODO

printf("New array: ");
printArray(array);
}
