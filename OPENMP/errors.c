#include <stdio.h>
#include <omp.h>
#define FUN(x) (x*x)
#define N 11

void NUM_THREAD()
{
	#pragma omp num_threads(8)
	printf("Hello from the thread: %d\n",omp_get_thread_num());
}

void SINGLE()
{
	#pragma omp parallel num_threads(2)
	{
		#pragma single
   		{
     		printf("me\n");
   		}
	}
}

void ALPHABET()
{
	omp_set_num_threads(4);
	int i;
	#pragma omp parallel private(i)
	{
    	int LettersPerThread = 26 / omp_get_num_threads();
    	int ThisThreadNum = omp_get_thread_num();
    	int StartLetter = 'a' + ThisThreadNum * LettersPerThread;
    	int EndLetter = 'a' + ThisThreadNum * LettersPerThread + LettersPerThread;
    	for (i=StartLetter; i<EndLetter; i++)
        	printf ("%c", i);
	}
	printf("\n");
}

void SMTH()
{	
#pragma omp parallel
   {
      printf("Message 1\n");
      printf("Message 2\n");
   }

}

void SQR()
{
  int idx;
  int main_var;

  #pragma omp parallel for
  for (idx = 0; idx < N+1; ++idx)
  {
    main_var = FUN(idx);
    printf("In thread %d idx = %d main_var = %d\n",
      omp_get_thread_num(), idx, main_var);
  }
  printf("Return to main thread with value of main_var = %d\n", main_var);
}


void main()
{
	NUM_THREAD();
	SINGLE();
	ALPHABET();
	SMTH();
        SQR();
}
