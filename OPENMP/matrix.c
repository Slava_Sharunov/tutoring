#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define N 3

int ** createMatrix() {
	int i;
	int ** matrix = (int**) calloc(N,sizeof(int*));
	int * m = (int*) calloc(N*N,sizeof(int));
	for (i = 0; i < N; i++) {
		matrix[i] = m+(i*N);
	}
	return matrix;
}

void fillMatrix(int ** matrix) {
	int i;
	srand(time(NULL));
	for (i = 0; i < N*N; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

int * CreateArrayOfMax(int ** matrix) //this function should return an array of all min elements of each row in a matrix
{
	int * array = (int*) calloc(N,sizeof(int));
	int i,j;
	//TODO - do not forget about OpenMP pragmas
	
	return array;    
}

int findMaxInArray(int * arr) //this function should return a max element of the array with min elements
{   
	int max = arr[0];
	int i;
	//TODO - do not forget about OpenMP pragmas
 
	return max;
}

void printMatrix(int ** matrix) {
	int i, j;
	for (i = 0; i < N; i++) {
		printf("[");
		for (j = 0; j < N; j++) {
			printf("%2d",matrix[i][j]);
			if (j != N-1)
				printf(",");
			else
				printf("]");
		}
		if (i != N-1)
			printf(",\n");
	}
	printf("\n");
}

void main()
{
	int i,j;
	
	int ** arr = (int**)createMatrix();
	fillMatrix(arr);
        printf("Matrix: \n");
	printMatrix(arr);	
	int * arrayMAX = CreateArrayOfMax(arr);
	printf("Array of MIN: \n");
	for (i=0;i<N;i++)
		printf("%d ",arrayMAX[i]);
	printf("\n");
	printf("MAX from MIN: %d\n",findMaxInArray(arrayMAX));
}
