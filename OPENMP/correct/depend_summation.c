#include <stdio.h>
#include <math.h>
#include <omp.h>

int Calculation(n)
{
  int i;
  int array[n];
  array[0]=1;
  array[1]=2;
  if (n>1)
    //#pragma omp parallel for // - it can not be parallelized due to data dependency
    for(i=2;i<n+1;i++)
      array[i]=6*array[i-1]-8*array[i-2]+i;
  else if (n==1)return 1;
  else return 0;
  return array[n];
}

void main()
{
  printf("Result: %d\n",Calculation(5));
}
