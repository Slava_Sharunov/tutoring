#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define N 5

int ** createMatrix() {
	int i;
	int ** matrix = (int**) calloc(N,sizeof(int*));
	int * m = (int*) calloc(N*N,sizeof(int));
	for (i = 0; i < N; i++) {
		matrix[i] = m+(i*N);
	}
	return matrix;
}

void fillMatrix(int ** matrix) {
	int i;
	srand(time(NULL));
	for (i = 0; i < N*N; i++) {
		matrix[0][i] = (rand()%10) - 5; //between -5 and 4
	}
}

int * CreateArrayOfMax(int ** matrix)
{
	int * array = (int*) calloc(N,sizeof(int));
	int i,j;
	#pragma omp parallel for private(i) //schedule(static,n)
	for (i=0;i<N;i++)
		{int min = matrix[i][0];
		for (j=1;j<N;j++)
			if (matrix[i][j]<min)
				min=matrix[i][j];
		array[i]=min;	
	    }		
	return array;    
}

int findMaxInArray(int * arr)
{   
	int max = arr[0];
	int i;
	#pragma omp parallel for
	for (i = 1; i < N; ++i)
		if (arr[i]>max)
			max=arr[i];
	return max;
}

void printMatrix(int ** matrix) {
	int i, j;
	for (i = 0; i < N; i++) {
		printf("[");
		for (j = 0; j < N; j++) {
			printf("%2d",matrix[i][j]);
			if (j != N-1)
				printf(",");
			else
				printf("]");
		}
		if (i != N-1)
			printf(",\n");
	}
	printf("\n");
}

void main()
{
	int i,j;
	
	int ** arr = (int**)createMatrix();
	fillMatrix(arr);
        printf("Matrix: \n");
	printMatrix(arr);	
	int * arrayMAX = CreateArrayOfMax(arr);
	printf("Array of MIN: \n");
	for (i=0;i<N;i++)
		printf("%d ",arrayMAX[i]);
	printf("\n");
	printf("MAX from MIN: %d\n",findMaxInArray(arrayMAX));
}
