#include <stdio.h>
#include <math.h>
#include <omp.h>
#define FUN(x) ((x)*(x)*(x)+2*(x))
#define N 100000000

double CalcIntegral(double a, double b)
{
  int i;
  double result=0, 
         h=(b - a) / N; //step for Rectangle method
 

  #pragma omp parallel for reduction (+: result)
  for(i = 0; i < N; i++)
  {
    result += FUN(a + h * (i + 0.5)); //Сompute result in the midpoint and add it to our sum
  }

  result *= h;

  return result;
}


int main(void)
{
  double integral;
  double w_time;
  
  w_time= omp_get_wtime();
  integral = CalcIntegral(0, 1);
  w_time=omp_get_wtime() - w_time;
  printf("The value of the integral is: %lf \n", integral);
  printf ("Execution time: %f\n", w_time);
  return 0;
}
