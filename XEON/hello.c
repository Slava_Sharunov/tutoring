//icc -o hello hello.c -fopenmp
//then we run it as usually
// export OMP_NUM_THREADS=10
//to run:  ./hello 
#include <stdio.h>
#include <omp.h>

void testThreads();

int main(int argc,char **argv)
{  
  testThreads();
  return 0;
}
  
void testThreads()
{
  int tCount,threadsMax,tid;   
#ifdef __MIC__
  printf("Running on Xeon Phi\n");
#else
  printf("Running on host\n");
#endif //__MIC__
  threadsMax=omp_get_max_threads();
  #pragma omp parallel default(none) private(tid) shared(tCount,threadsMax)
  {
    tid = omp_get_thread_num();
    #pragma omp critical
      printf("Hello from thread id %d\n",tid);
    #pragma omp barrier 
    #pragma omp master
    {
      tCount = omp_get_num_threads();
      printf("The number of threads is %d\n", tCount);
      printf("Maximum number of threads is %d\n", threadsMax);
      printf("================================\n");
    }
  }
}
