#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#include <hbwmalloc.h>
#define N 2000


float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int * createMatrix() {
	int i,j;
        //TODO
        //Now app will run only in HBM. 
	int * matrix;
	matrix = (int*) hbw_malloc(sizeof(int)*N*N);

	
	for (i = 0; i < N; i++) 
            for (j = 0; j < N; j++)
		matrix[i*N+j] = 0;
	
	return matrix;
}

void fillMatrix(int * matrix) {
	int i,j;
	for (i = 0; i < N; i++) 
            for (j=0; j < N; j++)
		matrix[i*N+j] = (rand()%10) - 5; //between -5 and 4
	
}

void printMatrix(int * matrix)
{
	int i,j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<N;j++)
			printf("%2d ",matrix[i*N+j]);
		printf("\n");
	}
}

int * matrixmult_usual(int * A, int * B) //normal multiplication
{
    int * C = (int*)createMatrix();
    //TODO
    int i,j,k;
   
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            for (k = 0; k < N; k++)
                C[i*N+j] += A[i*N+k]*B[k*N+j];
    return C;
}

int * matrixmult_spatial(int * A, int * B) //both matrices we "read" by rows
{
    int * C = (int*)createMatrix();
    //TODO
    int i,j,k;

    int residue;

    for (i = 0; i < N; i++)
        for (k = 0; k < N; k++) {
            residue = A[i*N+k];
            for (j = 0; j < N; j++)
                C[i*N+j] += residue*B[k*N+j];
        }
    return C;
}

void main()
{
	struct timeval stop, start;
	float elapsed;
	int result;
	int * a, *b, *c, *c_other;
	

 	a = (int*)createMatrix();
 	fillMatrix(a);
 	b = (int*)createMatrix();
 	fillMatrix(b);
        printf("A:\n");
	printMatrix(a);
	printf("B:\n");
	printMatrix(b);


 	gettimeofday(&start, 0);
	c = matrixmult_usual(a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
 	printf("Code executed for \"USUAL\" version in %f milliseconds.\n", elapsed); 

    	gettimeofday(&start, 0);
	c_other = matrixmult_spatial(a,b);
	gettimeofday(&stop, 0);
	elapsed = timedifference_msec(start, stop);
    	printf("Code executed for \"SPATIAL\" version in %f milliseconds.\n", elapsed);
	
        //TODO -- free the memory
        hbw_free(a);
        hbw_free(b);
        hbw_free(c);
        hbw_free(c_other);
}
