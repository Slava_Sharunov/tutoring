/*
compile:
!    mpicc -xMIC-AVX512 mpi_pi.c -o mpi_pi
! ____________________________________________________________________________
run:
!    ./mpi_pi

*/

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

const int    DEFAULT_INTERVAL = 100000;             
const int    MASTER_RANK     = 0;                  
const double ACTUAL_PI       = 3.141592653589793; 

double f( double x );
void   reportResults( double computedPi, int totalTasks,
          int totalSubints, double elapsedTime );

// ----------------------------------------------------------------------------

void main( int argc, char* argv[] ) {

    int numtasks;     // total MPI tasks (processes)
    int rank;         // rank number (process ID) of current process
    int totalSubintervals;   // total number of sub-intervals in Riemann sum
    int i;              // loop index; counts through sub-intervals
    double startTime;   // time at start of simulation
    double x;        // x coordinate at which we're evaluating function

    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &numtasks );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    if ( rank == MASTER_RANK ) {  
        startTime = MPI_Wtime();
        totalSubintervals = DEFAULT_INTERVAL;
    } 

       
    //TODO-Begin
    //Calculate a value of PI using rectangle method and MPI-techniques 
    
    double globalSum = ;
    double step = ;
    double halfStep = ;
    double localSum = ; 

    
    //TODO-End
    if ( rank == MASTER_RANK ) { 
        double stopTime = MPI_Wtime();
        double elapsedTimeInSeconds = stopTime - startTime;
        reportResults( globalSum, numtasks, totalSubintervals,
            elapsedTimeInSeconds );
    } 

    MPI_Finalize();

} 

// ----------------------------------------------------------------------------

void   reportResults( double computedPi, int totalTasks, int totalSubints,
                      double elapsedTime ) {

    double absError = fabs(  computedPi - ACTUAL_PI );
    double relError = absError / ACTUAL_PI;

    printf( "\n\t**********************************************\n"    );
    printf(   "\t         Computed value: %17.15f\n",    computedPi   );
    printf(   "\t         Absolute error: %7.2e\n",      absError     );
    printf(   "\t         Relative error: %7.2e\n",      relError     );
    printf(   "\t    Total MPI processes: %d\n",         totalTasks   );
    printf(   "\t    Total sub-intervals: %d\n",         totalSubints );
    printf(   "\t             Total time: %7.5f secs\n", elapsedTime  );
    printf(   "\t**********************************************\n\n"  );

}  
// ----------------------------------------------------------------------------

double f( double x ) {
    double value = 4.0 / ( 1.0 + x*x );
    return value;
}  
