/*
!    icc -qopenmp omp_hello.c -o omp_hello
!    ./omp_hello | sort
!    here flag sort is used exactly for soting of messages in increasing order
!
*/

#include <omp.h>
int main(){

   int i,id;

   printf("  0> Hello from Xeon Phi KNL\n");
   printf("  0> Processors: value of omp_get_num_procs()   is %3d\n", omp_get_num_procs()   );
   printf("  0> Threads:    value of omp_get_max_threads() is %3d\n", omp_get_max_threads() );

   #pragma omp parallel private(id)
   {
      id = omp_get_thread_num();
      printf("  => hello from thread id  %3.3d\n",  id);
   }

}
