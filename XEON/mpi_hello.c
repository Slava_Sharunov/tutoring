/*
!
!    compile:
!    mpicc -xMIC-AVX512 mpi_hello.c -o mpi_hello
! ____________________________________________________________________________
!    run:
!    ./mpi_hello | sort
!    here flag sort is used exactly for soting of messages in increasing order
*/

#include <mpi.h>
#include <stdio.h>
int main(int argc, char *argv[]){

   int ierr, nranks, irank, namelen;
   char name[MPI_MAX_PROCESSOR_NAME];

   ierr=MPI_Init(&argc, &argv);
   ierr=MPI_Comm_size(MPI_COMM_WORLD,  &nranks );
   ierr=MPI_Comm_rank(MPI_COMM_WORLD,  &irank  );
   ierr=MPI_Get_processor_name( name,  &namelen);

   if(irank==0) printf("  0> Hello from Xeon Phi KNL at processor %s\n", name);

   printf("  => hello from mpi process %3.3d\n", irank);

   ierr= MPI_Finalize();
}
